/*******************************************************************************
 * Copyright (c) 2017 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

loadModule("/Unittest");

function sleep(milliSeconds) {
	stop = java.lang.System.currentTimeMillis() + milliSeconds;
	while (java.lang.System.currentTimeMillis() < stop)
		;
}

/**
 * Create instances containing test functions.
 */
var modernTest = {

	// special marker to indicate this instance as unit test
	__unittest : '',

	// unit test description
	__description : 'Test class demonstrating modern testing',
	
	// globally ignore this instance and all its test methods
	// __ignore : 'tests ignored -> list ignored testcases',

	/**
	 * Method to be called before each test function. The name is not important, instead we are looking for our custom annotations
	 */
	setup : function() {
		// annotation indicating that this is the test setup method
		'@before';
		
		print("this is the test setup")
	},

	/**
	 * Method to be called after each test, even in case of errors.
	 */
	teardown : function() {
		'@after';
		
		print("this is the test teardown")
	},

	/**
	 * Method to be called once before all tests if this instance get executed.
	 */
	setupClass : function() {
		'@beforeclass';
		
		print("this is the testclass setup")
	},

	/**
	 * Method to be called once after all tests of this instance got executed.
	 */
	teardownClass : function() {
		'@afterclass';
		
		print("this is the testclass teardown")
	},
	
	/**
	 * Simple test case. The @test marker indicates it as a test.
	 */
	valid : function() {
		'@test';
		'@description(Working testcase)';

		assertTrue(true);
	},

	/**
	 * Instead of an @test annotation we could prefix the function name with 'test'.
	 */
	testInvalid : function() {
		'@description(testcase that fails)';

		assertTrue(false);
	},

	/**
	 * Even after throwing further tests of this instance will get executed.
	 */
	testError : function() {
		'@description(testcase throwing an exception)';

		throw "this does not work";
	},

	testError2 : function() {
		'@description(testcase throwing an exception)';

		java.lang.Class.forName("NonExistingClass");
	},

	/**
	 * Expect a java exception to be thrown. Test is pass when exception is detected.
	 */
	testExpectError : function() {
		'@description(testcase expecting an exception)';
		'@expect(java.lang.ClassNotFoundException)';

		java.lang.Class.forName("NonExistingClass");
	},

	/**
	 * This test will fail as the expected exception is not thrown.
	 */
	testExpectErrorButIsOK : function() {
		'@description(testcase expecting an exception)';
		'@expect(java.lang.ClassNotFoundException)';

	},

	/**
	 * Disable a test.
	 */
	testIgnored : function() {
		'@description(ignored testcase)';
		'@ignore(manually disabled testcase)';

		assertTrue(false);
	},

	/**
	 * Test failing due to exceeded execution time.
	 */
	testTimeout : function() {
		'@description(test running in a timeout)';
		'@timeout(100)';

		sleep(150);
	},
}
