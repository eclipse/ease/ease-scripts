/*******************************************************************************
 * Copyright (c) 2018 Aathman Tharmasanthiran and others. All rights reserved.
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License v1.0 which accompanies this distribution,
 * and is available at http://www.eclipse.org/legal/epl-v10.html
 * 
 * Contributors: Aathman Tharmasanthiran - initial API and implementation
 ******************************************************************************/

loadModule('/System/Git');
loadModule('/System/Resources');

var localRepo = "C:/New folder";
var remoteRepo = "https://github.com/AathmanT/Example-codes.git";
var username = "";
var password = "";

var gitRepo = clone(remoteRepo, localRepo, username, password);

var projectFileLocations = findFiles("*.project", localRepo, true);

for (var i = 0; i < (projectFileLocations.length); i++) {

	linkProject((projectFileLocations[i]).getParent());
}

var fileHandle = writeFile(localRepo + "/Test file.txt",
		"This is the test file added to test commit.");

fileHandle.close();

add(localRepo, "Test file.txt");

commit(localRepo, "Added a test file", "AathmanT aathmant@gmail.com");

push(localRepo);
