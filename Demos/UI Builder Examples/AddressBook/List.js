/*******************************************************************************
 * Copyright (c) 2019 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/
loadModule('/System/Resources');
loadModule('/System/UI Builder');
loadModule('/System/UI');

createView("Phone Numbers")

var tableViewer = createTableViewer(readAddresses())
createViewerColumn(tableViewer, "Name", createLabelProvider("getProviderElement().firstName + ' ' + getProviderElement().lastName"))
createViewerColumn(tableViewer, "Phone", createLabelProvider("getProviderElement().phone"))

executeUI("tableViewer.setInput(readAddresses())");


function readAddresses() {
	var addresses = new Array();
	
	var files = findFiles("*.address");
	for (index in files) {
		var data = readFile(files[index]);
		var address = JSON.parse(data);
		addresses.push(address);
	}
	
	return addresses;
}