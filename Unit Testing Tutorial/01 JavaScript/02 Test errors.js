/*******************************************************************************
 * Copyright (c) 2015 Christian Pontesegger and others.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     Christian Pontesegger - initial API and implementation
 *******************************************************************************/

loadModule("/Unittest", false);

// test containing assertions that fail
startTest("invalid assertions", "a test containing invalid assertions");
assertTrue(true);
assertTrue(false);
assertFalse(true);
assertFalse(false);
endTest();

// manually create an error
startTest("failure", "test throwing a failure");
error("test broken, stop execution here");

// an exception would also create an error
throw new java.lang.Exception("code exception");
endTest();

startTest("never to be reached");
// not being reached as the error above terminates test file execution
endTest();
