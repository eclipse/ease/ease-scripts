loadModule("/Unittest");

var ModernTest = {

	__unittest : '',
	__description : 'Test class demonstrating modern testing',

	testValid : function() {
		'@description(Working testcase)';

		assertTrue(true);
	},

	testInvalid : function() {
		'@description(testcase that fails)';

		assertTrue(false);
	},

	testError : function() {
		'@description(testcase throwing an exception)';

		throw "this does not work";
	},

	testExpectError : function() {
		'@description(testcase expecting an exception)';
		'@expect(java.lang.Throwable)';

		throw new java.lang.Throwable("this does not work");
	},

	testExpectErrorButNotThrown : function() {
		'@description(testcase expecting an exception but not thrown from test)';
		'@expect(java.lang.Throwable)';

	},

	testIgnored : function() {
		'@description(testcase expecting an exception)';
		'@ignore(something)';

		print("This test should not be executed")
		assertTrue(false);
	},

	testTimeout : function() {
		'@description(running into timeout)';
		'@timeout(500)';

		var start = java.lang.System.currentTimeMillis();
		while (java.lang.System.currentTimeMillis() - start < 1000);
	}
}
