import unittest

class Test(unittest.TestCase):

    def setUp(self):
        print("\t\ttest setup")
        pass

    def tearDown(self):
        print("\t\ttest teardown")
        pass

    def testValid(self):
        self.assertTrue(True)
        pass

    def testInvalid(self):
        self.assertTrue(False)
        pass
    
    def testError(self):
        raise(BaseException("does not work"))
    
    @unittest.expectedFailure
    def testExpectError(self):
        raise(BaseException("expected exception"))

    @unittest.expectedFailure
    def testExpectErrorButIsOK(self):
        pass

    @unittest.skip("demonstrating skipping")
    def testIgnored(self):
        self.assertTrue(False)
        pass
    
if __name__ == "__main__":
    print("running from python directly")
    unittest.main()